# Analyse de faisabilité de conversion de formulaire EFL / EL

## Contexte (Décembre 2020)

Le contexte est à cheval entre 

-	le SDI : vision à long terme avec un format commun/pivot unifié + utilisation d’oppus à étudier
-	le projet POP : partage du travail des rédaction. Là-dessus les rédaction EFL et EL se sont déjà mis d’accord pour partager le travail de rédaction et donc échanger des formulaire entre EL et EFL

Ici nous sommes dans le contexte POP, mais sans perdre de vue la cible SDI (notre analyse permet de débroussailler les choses pour avoir une vision plus clair d’un schéma unifié pour les formulaires)


Fatima a réussi à en convertir 6 formulaire EFL en formulaires EL à la main (copié/collé), Matthieu travaille en parallèle sur la possibilité d’une moulinette pour faciliter ce travail.

A ce stade, les conversion manuelle de Fatima ont fait ressortir que ça ne marche pas trop mal, mais qu’il y des lacunes dans la DTD EL qui est un peu trop fermée quant aux possibilités de structuration qu’offre la DTD EFL.
Fatima a donc été obligé de ruser en détournant ces lacunes :

-	Reformulation éditoriales : changer des phrases, en répéter certaines, restructurer différemment, mettre des indications supplémentaire pour l’utilisateur, etc.
-	Créer 2 formulaires EL là où il n’y en avait qu’un seul aux EFL

## Besoin de faire évoluer la DTD EL

On voulait donc voir avec Paul s’il était possible d’ouvrir la DTD EL afin de permettre ce qui est possible aux EFL et donc éviter toutes ces adaptations (qui rende d’ailleurs le formulaire moins pratique pour l’utilisateur)
Fatima peut désactiver la validation DTD dans frame mais elle ne peut pas publier les documents non valides qui sont bloqués plus loin dans la chaîne de traitement qui valide aussi le contenu (format TRA).
La question qu’on se posait c’est surtout de savoir si le front pouvait afficher ces formulaires plus ouverts sans modification et que finalement c’est juste un problème de validation DTD dans la chaîne de publication.
On a donc demander à Paul s’il était possible de tester cela pour rapidement, sans changer une ligne de code mais juste en adaptant les DTD. Histoire qu’on sache si le problème se situe uniquement côté DTD ou s’il y avait autre chose à adapter.

La réponse de Paul : 

-	Déjà la chaîne de publication EL n’a pas de branche de test sur laquelle on pourrait tester cela. Il y a un tuyau commun et ensuite ça arrive soit sur test.validation soit en prod.
-	Donc faire ce test doit être traité comme une évolution en mode « maintenance évolutive »
    -	A noter que toutes les maintenance évolutives avait été mises en standbye depuis longtemps pour ne pas investir plus de temps sur le projet.
-	Une fois le test réalisé et si tout est ok en front, alors on pourra valider la solution pour tous les formulaires EL
-	Si ce n’est pas le cas on pourra toujours revenir en arrière, il est envisageable de faire une maintenance évolutive en mode « POC » (à défaut de chaîne de publication de test qui servirait à ça)
-	Paul pense qu’il faudrait en profiter pour aligner les choses côté Dalloz dans la mesure où certains formulaire EL sont distribués chez Dalloz
    -	On aimerait éviter que cela devienne un gros chantier mais oui il faut une étude d’impact côté Dalloz

C’est donc un peu plus compliqué que ce qu’on imaginait, mais peut-être pas forcément grand-chose en terme de temps : le travail se situe entre Paul (partie rédaction) et Ahmed (partie publication).


Pourrait-on soumettre une jira avec ces demandes précises (avec exemples, etc.) afin d’obtenir un estimation.
A partir de là on pourra décider ce qu’il convient de faire : arrêter tout / lancer le dév (si moins de 5j) / Pitcher cette demande
