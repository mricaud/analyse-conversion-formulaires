<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
  xmlns:els="http://www.lefebvre-sarrut.eu/ns/els"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:saxon="http://saxon.sf.net/"
  exclude-result-prefixes="#all"
  version="3.0"
  >
  
  <xd:doc scope="stylesheet">
    <xd:desc>
      <xd:p>XSLT de conversion des formulaires EFL en formulaires EL</xd:p>
      <xd:p>Les définitions de DTD proviennent ici d'une DTD inférée des données EFL</xd:p>
    </xd:desc>
  </xd:doc>
  
  <xsl:include href="formEFL-to-formEL.lib.xsl"/>
  
  <!--Il faut utiliser ce DOCTYPE pour que les fichiers s'ouvrent dans frame
    Utiliser catalog.xml pour utiliser une copie local de cette DTD-->
  <xsl:output doctype-system="\\nas\WORKFLOW_test\DP\DTD\dpform2-xml.dtd"/>
  
  <!--==================================================================================================-->
  <!--MAIN-->
  <!--==================================================================================================-->
  
  <xsl:template match="/">
    <xsl:variable name="step" select="." as="document-node()"/>
    <xsl:variable name="step" as="document-node()">
      <xsl:document>
        <xsl:apply-templates select="$step" mode="prepareEFLform"/>
      </xsl:document>
    </xsl:variable>
    <xsl:variable name="step" as="document-node()">
      <xsl:document>
        <xsl:apply-templates select="$step" mode="formEFL2formEL"/>
      </xsl:document>
    </xsl:variable>
    <xsl:variable name="step" as="document-node()">
      <xsl:document>
        <xsl:apply-templates select="$step" mode="optimizeFormEL"/>
      </xsl:document>
    </xsl:variable>
    <xsl:sequence select="$step"/>
  </xsl:template>
  
  <!--==================================================================================================-->
  <!--prepareEFLform-->
  <!--==================================================================================================-->
  <!--====================================-->
  <!--LST/ITEM-->
  <!--====================================-->
  
  <!--Remise LST autour ITEM si manquant-->
  <xsl:template match="*[not(self::LST)][ITEM]" mode="prepareEFLform">
    <xsl:copy>
      <xsl:apply-templates select="@*" mode="#current"/>
      <xsl:for-each-group select="node() except text()[not(normalize-space())]"
        group-adjacent="name() = 'ITEM'">
        <xsl:choose>
          <xsl:when test="current-group()[1]/self::ITEM">
            <LST>
              <xsl:apply-templates select="current-group()" mode="#current"/>
            </LST>
          </xsl:when>
          <xsl:otherwise>
            <xsl:apply-templates select="current-group()" mode="#current"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:for-each-group> 
    </xsl:copy>
  </xsl:template>
  
  <!--====================================-->
  <!--GRCHOIXT + CHOIXT-->
  <!--====================================-->
  
  <!--EFL : GRCHOIXT + CHOIXT permets d'insérer des choix dans le texte, souvent pour distinguer dans la phrase du pluriel/singlulier ("la ou les résolutions")
  ou féminin/masculin (Monsieur le ... ou Madame la ...). ou booléen ("oui/non")
  A noter les GRCHOIXT sont toujours de TYPE="U" dans les données, ce sont donc des boutons radio (1 seul choix possible)
  Ici nous allons distinguer 3 cas : 
  CAS 1 : les CHOIXT ne contiennent que du texte court (ex : "valeur 1" et "valeur 2") => on peut remplacer par une précision "valeur 1 / valeur 2"
  CAS 2 : les CHOIXT ne contiennent que du texte mais trop long pour afficher "valeur 1 / valeur 2" => créer un blocOption 
  CAS 3 : les CHOIXT ne contiennent du texte + une variable : 
    - si on est déjà dans une option : on peut la dupliquer en faisant varier la phrase en extrayant le texte du CHOIXT
    - sinon créer un blocOptions avec les 2 phrases
    
  NB : quand on dit "que du texte" on veut dire sans VAR en fait
  -->
  
  <!--<!ELEMENT GRCHOIXT (SEL?,(AT|GRCHOIXT|LIB|CHOIXT)*,VAR?)>-->
  
  <!--On exclu les cas non connus-->
  <xsl:template match="GRCHOIXT[*[not(self::CHOIXT or self::LIB)] or text()[normalize-space(.)]]"
    mode="prepareEFLform" priority="1">
    <xsl:message terminate="yes">ERREUR : GRCHOIXT avec autre chose que des CHOIX, à analyser - <xsl:value-of select="saxon:path(.)"/></xsl:message>
    <xsl:next-match/>
  </xsl:template>
  
  <!--CAS 1 : pas de VAR dans les CHOIXT : remplacer par Precision avec /-->
  <!--Si on mets un PI dans le XML on peut aussi forcer cela, on perdra les CHOIXT/VAR s'il y en a-->
  <xsl:template match="GRCHOIXT[not(CHOIXT/VAR) or processing-instruction('efl2el') = 'force-precision-slash']"
    mode="prepareEFLform">
    <VAR AREA="NON" CODE="{@CODE}">
      <xsl:copy-of select="processing-instruction() | comment()"/>
      <xsl:processing-instruction name="efl2el">_CHOIXT simple-conversion</xsl:processing-instruction>
      <xsl:for-each select="CHOIXT">
        <xsl:apply-templates mode="#current"/>
        <xsl:if test="not(position()=last())">
          <xsl:text>/</xsl:text>
        </xsl:if>
      </xsl:for-each>
    </VAR>
  </xsl:template>
  
  <xsl:template match="GRCHOIXT[processing-instruction('efl2el') = 'force-precision-slash']//VAR" mode="prepareEFLform">
    <xsl:apply-templates mode="#current"/>
  </xsl:template>
  
  <!--CAS 3 : il y a des VAR dans les CHOIX et on est déjà dans un CHOIX[N] : on duplique-->
  <xsl:template match="*[els:isCHOIX(.)][.//GRCHOIXT[CHOIXT/VAR]]" mode="prepareEFLform">
    <xsl:variable name="self" as="element()" select="."/>
    <!--GRCHOIXT qui ne sont pas dans un sous CHOIX[N] mais directement dans celui-ci,
      si il y a une LETTRE entre les 2 on les exclu aussi-->
    <xsl:variable name="GRCHOIXT.content" as="element()*"
      select=".//GRCHOIXT[not(ancestor::LETTRE[ancestor::*[. is $self]])][ancestor::*[els:isCHOIX(.)][1] is $self]"/>
    <xsl:choose>
      <xsl:when test="count($GRCHOIXT.content) != 0">
        <xsl:for-each select="$GRCHOIXT.content/CHOIXT">
          <xsl:apply-templates select="$self" mode="prepareEFLform.duplicateCHOIX">
            <xsl:with-param name="duplicateByThisCHOIXT" as="element()" select="." tunnel="true"/>
          </xsl:apply-templates>
        </xsl:for-each>
      </xsl:when>
      <xsl:otherwise>
        <xsl:next-match/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template match="GRCHOIXT" mode="prepareEFLform" priority="-1">
    <xsl:message terminate="yes">[ERROR] GRCHOIXT non converti : <xsl:value-of select="saxon:path(.)"/></xsl:message>
  </xsl:template>
  
  <!--==============================-->
  <!--START SUB MODE duplicateCHOIX-->
  <!--==============================-->
  
  <xsl:mode name="prepareEFLform.duplicateCHOIX" on-no-match="shallow-copy"/>
  
  <xsl:template match="*[els:isCHOIX(.)]" mode="prepareEFLform.duplicateCHOIX">
    <xsl:param name="duplicateByThisCHOIXT" as="element()" tunnel="true"/>
    <xsl:copy>
      <xsl:if test="count($duplicateByThisCHOIXT) != 0">
        <xsl:attribute name="_duplicate" select="'true'"/>
      </xsl:if>
      <xsl:apply-templates select="@* | node()" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <!--Le content de chaque GRCHOIXT/CHOIXT est mis dans la phrase-->
  <xsl:template match="GRCHOIXT" mode="prepareEFLform.duplicateCHOIX">
    <xsl:apply-templates mode="#current"/>
  </xsl:template>
  
  <xsl:template match="CHOIXT" mode="prepareEFLform.duplicateCHOIX">
    <xsl:param name="duplicateByThisCHOIXT" as="element()" tunnel="true"/>
    <xsl:choose>
      <xsl:when test="self::CHOIXT is $duplicateByThisCHOIXT">
        <xsl:processing-instruction name="efl2el">_CHOIXT applied-start</xsl:processing-instruction>
        <xsl:apply-templates mode="#current"/>
        <xsl:processing-instruction name="efl2el">_CHOIXT applied-end</xsl:processing-instruction>
      </xsl:when>
      <xsl:otherwise>
        <xsl:processing-instruction name="efl2el">_CHOIXT deleted</xsl:processing-instruction>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <!--==============================-->
  <!--END SUB MODE duplicateCHOIX-->
  <!--==============================-->
  
  <xsl:template match="node() | @*" mode="prepareEFLform" priority="-2">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <!--==================================================================================================-->
  <!--MODE formEFL2formEL-->
  <!--==================================================================================================-->
  
  <!--NOTE : les éléments de DTD sont récupérés d'une DTD générée automatiquement à partir de EFL_societes.xml complet-->
  
  <!-- Acte : <!ELEMENT ACTE (TACTE,RATTACH?,(AT|SEL|SELPREALABLE)*,(ASSIGN|CTRAT|PV|STAT|DIVERS*),NOTEPV*,LETTRE*,GRCHOIX1*,TR?,POUVOIR*)>-->
  <xsl:template match="ACTE[els:getACTEtypeEFL(.) = 'lettre']" mode="formEFL2formEL">
    <xsl:variable name="LETTRE" select="LETTRE" as="element()"/>
    <DPFORM2 Type="2" Vue="Saisie"
      versionEL="FIXME"  MAJNUM="FIXME" DATE="{@DATEMAJCODIF}"
      GB="DP05" MR="Off" 
      Titre="{TACTE/T}"
      Theme="FIXME"
      Theme2="FIXME"
      Theme3="FIXME"
      >
      <!--FIXME : Theme, Theme2 et Theme3 sont les niveaux (PART/SPART/NIV3) d'imbrication de l'acte, on peut les récupérer si on a l'ouvrage entier.-->
      <Notfiche>FIXME</Notfiche>
      <!--FIXME : aucune indication de numéro de fiche côté EFL-->
      <xsl:apply-templates mode="#current"/>
    </DPFORM2>
  </xsl:template>
  
  <!--Titre Acte : <!ELEMENT TACTE (T,RATTACH*)>-->
  <xsl:template match="TACTE" mode="formEFL2formEL">
    <xsl:apply-templates mode="#current"/>
  </xsl:template>
  
  <xsl:template match="TACTE/T" mode="formEFL2formEL">
    <Titrefiche>
      <xsl:apply-templates mode="#current"/>
    </Titrefiche>
  </xsl:template>
  
  <!--Avertissement : <!ELEMENT AT (TAT?,(AL|LST)*)>-->
  <xsl:template match="AT" mode="formEFL2formEL">
    <xsl:apply-templates mode="#current"/>
  </xsl:template>
  
  <xsl:template match="AT/TAT | AT/AL" mode="formEFL2formEL">
    <Alobs>
      <xsl:apply-templates mode="#current"/>
    </Alobs>
  </xsl:template>
  
  <xsl:template match="DAT" mode="formEFL2formEL">
    <Date>
      <xsl:apply-templates mode="#current"/>
    </Date>
  </xsl:template>
  
  <!--
    Structure d'un lettre EFL : <!ELEMENT LETTRE ((AL|GRCHOIX1|LIB|LST|SEL|TITRE|TXLETTRE|COMPL|DAT|DEST|EXP|TLETTRE)*,SIGN?,ANX?)>
    qu'on pourrait d'après les exemples ré-écrire :
      LETTRE : LIB, (EXP, DEST)*, COMPL, DAT, TITRE, TXLETTRE
    Aux EL <lettre> n'existe pas (c'est un groupe d'éléments sous DPFORM2[@Type='2'] - après quelques éléments introductifs) 
    avec un sequence de types :
      "vitual letter" : (Expediteur, Destinataire)*, Date, TypeLettre, Al
    Les mapping implicites ici sont : 
                                       EFL       EL
                                      COMPL    TypeLettre
                                       DAT      Date
                                      TITRE      AL
                                      TXLETTRE  (void)
  -->
  
  <!--<!ELEMENT COMPL (VAR?,L*,(GRCHOIX1|GRCHOIX2|GRCHOIXT|LVAR*))>-->
  <xsl:template match="LETTRE/COMPL[not(text()[normalize-space()])][count(*) = 1][L]" mode="formEFL2formEL">
    <Typelettre>
      <xsl:apply-templates select="L/node()" mode="#current"/>
    </Typelettre>
  </xsl:template>
  
  <!--<!ELEMENT TITRE (#PCDATA|AL|SEL)*>-->
  <xsl:template match="LETTRE/TITRE" mode="formEFL2formEL">
    <xsl:apply-templates mode="#current"/>
  </xsl:template>
  
  <!--Contenur EFL sans équivalent EL (à plat)-->
  <!--<!ELEMENT LETTRE ((AL|GRCHOIX1|LIB|LST|SEL|TITRE|TXLETTRE|COMPL|DAT|DEST|EXP|TLETTRE)*,SIGN?,ANX?)>-->
  <!--<!ELEMENT TXLETTRE (GRCHOIXT?,(AL|AT|CITART|GRCHOIX1|GRCHOIX2|LST|SEL|TABLEAU|TMIXTE|POLI)+)>-->
  <xsl:template match="LETTRE | TXLETTRE" mode="formEFL2formEL">
    <xsl:apply-templates mode="#current"/>
  </xsl:template>
  
  <!--====================================-->
  <!--ELEMENTS COMMUNS BLOCK-->
  <!--====================================-->
  
  <xsl:template match="AL | OUVAL" mode="formEFL2formEL">
    <Al Typemaj="0">
      <xsl:apply-templates mode="#current"/>
    </Al>
  </xsl:template>
  
  <!--<!ELEMENT POLI (AL)>-->
  <xsl:template match="POLI" mode="formEFL2formEL">
    <xsl:apply-templates mode="#current"/>
  </xsl:template>
  
  <xsl:template match="SIGN[L]" mode="formEFL2formEL">
    <xsl:apply-templates mode="#current"/>
  </xsl:template>
  
  <xsl:template match="SIGN/L | SIGN[not(L)]" mode="formEFL2formEL">
    <Signature Typemaj="0">
      <xsl:apply-templates mode="#current"/>
    </Signature>
  </xsl:template>
  
  <!--Requête de sélection dans la base AJ-->
  <xsl:template match="SEL" mode="formEFL2formEL">
    <xsl:comment><xsl:value-of select="serialize(.)"/></xsl:comment>
  </xsl:template>
  
  <!--Les listes sont à plat aux EL-->
  
  <!--<!ELEMENT ITEM (GRCHOIXT?,(AL|AT|GRCHOIX1|GRCHOIX2|GRCHOIX3|LIB|LST|SEL|VAR)*,(CITATION|GRCHOIX4|GRCHOIX5)?)>-->
  <xsl:template match="LST" mode="formEFL2formEL">
    <xsl:apply-templates mode="#current"/>
  </xsl:template>
  
  <xsl:template match="LST[(@PRES, 'RIEN')[1] = ('TIR', 'RIEN')]/ITEM" mode="formEFL2formEL">
    <Altiret Typemaj="0">
      <!--suppression d'indentation en trop ici-->
      <xsl:apply-templates select="text()[normalize-space(.)] | *" mode="#current"/>
    </Altiret>
  </xsl:template>
  
  <xsl:template match="LST[(@PRES, 'RIEN')[1] = ('TIR', 'RIEN')]/ITEM/AL" mode="formEFL2formEL">
    <xsl:apply-templates mode="#current"/>
  </xsl:template>
  
  <!--====================================-->
  <!--ELEMENTS COMMUNS INLINE-->
  <!--====================================-->
  
  <!--Gras-->
  <xsl:template match="MOTREP" mode="formEFL2formEL">
    <B>
      <xsl:apply-templates mode="#current"/>
    </B>
  </xsl:template>
  
  <!--Exposants-->
  <xsl:template match="E" mode="formEFL2formEL">
    <E>
      <xsl:apply-templates mode="#current"/>
    </E>
  </xsl:template>
  
  <!--====================================-->
  <!--EXPEDITEUR-->
  <!--====================================-->
  
  <!--<!ELEMENT EXP ((LIB|SEL|SELPREALABLE)?,(GRCHOIX1|GRCHOIX2|L|LVAR)*)>-->
  <xsl:template match="EXP" mode="formEFL2formEL">
    <xsl:apply-templates mode="#current"/>
  </xsl:template>
  
  <xsl:template match="EXP/L" mode="formEFL2formEL">
    <Alexpediteur Typemaj="0">
      <xsl:apply-templates mode="#current"/>
    </Alexpediteur>
  </xsl:template>
  
  <!--Quand un EXP/L ne contient qu'un variable (→ Precision) la DTD EL ne permets pas d'avoir une Precision sans parent, 
    on ajoute donc Expediteur-->
  <xsl:template match="EXP/L[not(text()[normalize-space()])][VAR[els:varCode-to-PrecisionCarnet(@CODE) != '']]"
    mode="formEFL2formEL" priority="1">
    <Expediteur Typemaj="0">
      <xsl:apply-templates mode="#current"/>
    </Expediteur>
  </xsl:template>
  
  <!--====================================-->
  <!--DESTINATAIRE-->
  <!--====================================-->
  
  <!--<!ELEMENT DEST ((SEL|SELPREALABLE)?,(GRCHOIX1|L)*)>-->
  <xsl:template match="DEST" mode="formEFL2formEL">
    <xsl:apply-templates mode="#current"/>
  </xsl:template>
  
  <xsl:template match="DEST/L" mode="formEFL2formEL">
    <Aldestinataire Typemaj="0">
      <xsl:apply-templates mode="#current"/>
    </Aldestinataire>
  </xsl:template>
  
  <!--Quand un DEST/L ne contient qu'un variable (→ Precision) la DTD EL ne permets pas d'avoir une Precision sans parent,
    on ajoute donc Destinataire-->
  <xsl:template match="DEST/L[not(text()[normalize-space()])][VAR[els:varCode-to-PrecisionCarnet(@CODE) != '']]" 
    mode="formEFL2formEL" priority="1">
    <Destinataire Typemaj="0">
      <xsl:apply-templates mode="#current"/>
    </Destinataire>
  </xsl:template>
  
  <!--====================================-->
  <!--VAR-->
  <!--====================================-->
  
  <xsl:template match="VAR" mode="formEFL2formEL">
    <Precision Typemaj="0" Vuepropagation="Oui" 
      Propagation="FIXME: A Y ou Z" 
      Numpropagation="FIXME : Z1,Z4,Z5,Z7,A1,A2,A7,A,Y1,Y5,Y3,Y4,Y7,Z" 
      Delimitation="Oui" Vue="Saisie">
      <xsl:choose>
        <xsl:when test="els:varCode-to-PrecisionCarnet(@CODE) = ''">
          <xsl:apply-templates mode="#current"/>
        </xsl:when>
        <!--FIXME-->
        <xsl:when test="starts-with(els:varCode-to-PrecisionCarnet(@CODE), 'FIXME:')">
          <xsl:attribute name="Carnet" select="els:varCode-to-PrecisionCarnet(@CODE)"/>
          <xsl:apply-templates mode="#current"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="Carnet" select="els:varCode-to-PrecisionCarnet(@CODE)"/>
        </xsl:otherwise>
      </xsl:choose>
    </Precision>
  </xsl:template>
  
  <!--====================================-->
  <!--GRCHOIX ...-->
  <!--====================================-->
  
  <!--<!ELEMENT GRCHOIX1 (LIB?,SEL?,(AT|CHOIX1)+)>-->
  <xsl:template match="*[els:isGRCHOIX(.)][els:eflGRCHOIXisMultiple(.)]" mode="formEFL2formEL">
    <xsl:apply-templates mode="#current">
      <xsl:with-param name="isMultiple" as="xs:boolean" select="true()" tunnel="true"/>
    </xsl:apply-templates>
  </xsl:template>
  
  <xsl:template match="*[els:isGRCHOIX(.)][not(els:eflGRCHOIXisMultiple(.))]" mode="formEFL2formEL">
    <xsl:variable name="name" as="xs:string" 
      select="if(count(ancestor::*[els:isGRCHOIX(.)]) ge 1)
      then('Blocsousoptions') else('Blocoptions')"/>
    <xsl:element name="{$name}">
      <xsl:attribute name="Delimitation" select="'Oui'"/>
      <xsl:apply-templates mode="#current"/>
    </xsl:element>
  </xsl:template>
  
  <!--<!ELEMENT CHOIX1 (LIB,ANX*,(PSSIGN|(TXLETTRE,FIN))?,(GRCHOIXT|SSART*),SEC?,
  (AL|ART|AT|CHAP|CITART|DECI|GRCHOIX2|ITEM|ITEMVAR|L|LST|SART|SEL|TABLEAU|TITRE|VAR)*,
  (GRSIGN|LVAR|PRESENT|SSIGN|VOTE|LETTRE*))>-->
  <!--Cas des exemples de Fatima 
    <!ELEMENT CHOIX1 (LIB,(GRCHOIX2|ITEM|LETTRE|LST|DECI)?,AT?,AL*)>: 
      - LST avec ITEM
      - ITEM directement
      - DECI avec NO + TDECI (saisissable) + TXDECI (saisissable)
        - cf. Z51966_Texte_des_resolutions_proposees.xml : 
          il y a plusieurs balises qui sautent : TTR,  TDECI/NO. TDECI/T => <Resolution>
      - GRCHOIX2
        - cf. Lettre de l'Organe dirigeant en vue de la consultation de chaque associé
  --> 
  
  <!--====================================-->
  <!--Blocajouts / Blocsousajouts-->
  <!--====================================-->
  <!--
    - Attention : Il n’y a qu’un seul élément « Ajout » par Blocajouts (cf. doc utilisateur)
    - l'ajout est définit dans un item de liste côté EFL
    - Il n'y a jamais plus d'un item dans ces type de liste :
      => //GRCHOIX{N}[@TYPE = 'M']/CHOIX{N}/LST/ITEM[LIB][2] ne renvoit aucun éléments dans societe.xml
      (même s'il peut y avoir des listes "normales" par ailleurs, plus bas dans la sous-arbo)
  -->
  <xsl:template match="*[els:isGRCHOIX(.)][els:eflGRCHOIXisMultiple(.)]/*[els:isCHOIX(.)]"
    mode="formEFL2formEL">
    <xsl:variable name="name" as="xs:string" 
      select="if(count(ancestor::*[els:isGRCHOIX(.)]) gt 1)
      then('Blocsousajouts') else('Blocajouts')"/>
    <xsl:element name="{$name}">
      <xsl:attribute name="Delimitation" select="'Oui'"/>
      <xsl:choose>
        <!--Si un répétition est prévue dans le format EFL on ajout un commentaire pour l'indiquer-->
        <xsl:when test="(LST/ITEM/@REP, ITEM/@REP) = 'OUI'">
          <xsl:variable name="commentaireId" select="'FIXME_' || generate-id(.)" as="xs:string"/>
          <xsl:apply-templates mode="#current">
            <xsl:with-param name="commentaireId" as="xs:string" select="$commentaireId" tunnel="true"/>
          </xsl:apply-templates>
          <Commentaire ID="{$commentaireId}" Num="4" Vue="Saisie">ajouter autant de FIXME que nécessaire.</Commentaire>
        </xsl:when>
        <xsl:otherwise>
          <xsl:apply-templates mode="#current"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:element>
  </xsl:template>
  
  <!--====================================-->
  <!--Blocoptions / Blocsousoptions-->
  <!--====================================-->
  
  <!--Rien à faire avec le CHOIX ici, c'est le LIB qui va servir à générer <option>, on a alors une succession de
  option/Aloption, les uns après les autres aux EL-->
  <xsl:template match="*[els:isGRCHOIX(.)][not(els:eflGRCHOIXisMultiple(.))]/*[els:isCHOIX(.)]"
    mode="formEFL2formEL">
    <xsl:apply-templates mode="#current"/>
  </xsl:template>
  
  <!--Label du choix - là où on clique pour l'activer)-->
  <xsl:template match="*[els:isGRCHOIX(.)][els:eflGRCHOIXisMultiple(.)]/*[els:isCHOIX(.)]/LIB" mode="formEFL2formEL">
    <Ajout Typemaj="0">
      <xsl:apply-templates mode="#current"/>
    </Ajout>
  </xsl:template>
  
  <!--Label du choix - là où on clique pour l'activer)-->
  <xsl:template match="*[els:isGRCHOIX(.)][not(els:eflGRCHOIXisMultiple(.))]/*[els:isCHOIX(.)]/LIB" mode="formEFL2formEL">
    <Option Typemaj="0">
      <xsl:apply-templates mode="#current"/>
    </Option>
  </xsl:template>
  
  <xsl:template match="*[els:isCHOIX(.)]/LST[LIB] | *[els:isCHOIX(.)]/LST[LIB]/ITEM" mode="formEFL2formEL">
    <xsl:param name="isMultiple" as="xs:boolean" tunnel="true"/>
    <xsl:choose>
      <xsl:when test="$isMultiple">
        <xsl:choose>
          <xsl:when test="preceding-sibling::*[name() = current()/name(.)]">
            <ERROR at="{saxon:path()}">[FATAL] Item de liste multiple non attendu dans un GRCHOIX simple directement sous le CHOIX[N]</ERROR>
          </xsl:when>
          <xsl:otherwise>
            <!--Suppression : Quand le choix est multiple, il n'y a qu'une liste 
              avec un seul item qui ne sert qu'à englober le contenu-->
            <xsl:apply-templates mode="#current"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <xsl:next-match/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template match="*[els:isCHOIX(.)]/LST/ITEM/AL[last()]" priority="1" mode="formEFL2formEL">
    <xsl:param name="commentaireId" as="xs:string?" tunnel="true"/>
    <xsl:choose>
      <xsl:when test="$commentaireId">
        <!--<Al> les AL sous ITEM ne sont jamais copiés, cf. traitement des listes-->
          <xsl:apply-templates mode="#current"/>
          <xsl:text> </xsl:text>
          <Appelcommentaire Typemaj="0" Idref="{$commentaireId}" Num="2" Vue="Saisie"/>
        <!--</Al>-->
      </xsl:when>
      <xsl:otherwise>
        <xsl:next-match/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <!--Suppression de tous les libellés non-matchés qui ne sont utiles que dans le contexte EFL 
    (pour le logiciel AJ et pour guider la saisie en mode "questionnaire" sur le web)-->
  <xsl:template match="LIB" mode="formEFL2formEL" priority="-1">
    <xsl:comment><xsl:sequence select="serialize(.)"/></xsl:comment>
  </xsl:template>
  
  <!--<xsl:template match="*[els:isGRCHOIX(.)]/LIB" mode="formEFL2formEL">
    <Al>FIXME <xsl:apply-templates mode="#current"/></Al>
  </xsl:template>-->
  
  <!--Suppression du libellé qui ne sert qu'à aider le rédacteur EFL (fixme?)-->
  <!--<xsl:template match="*[els:isCHOIX(.)]/LST/ITEM/LIB | *[els:isCHOIX(.)]/ITEM/LIB" mode="formEFL2formEL">
    <xsl:param name="isMultiple" as="xs:boolean" tunnel="true"/>
    <xsl:choose>
      <xsl:when test="$isMultiple">
        <!-\-Suppression-\->
        <Al>FIXME <xsl:apply-templates mode="#current"/></Al>
      </xsl:when>
      <xsl:otherwise>
        <xsl:next-match/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>-->
  
  <!--====================================-->
  <!--DEFAULT COPY-->
  <!--====================================-->
  
  <!--Ajout prefixe "_" au nom des balises non matchée pour bien les repérer-->
  <xsl:template match="*" mode="formEFL2formEL" priority="-2">
    <xsl:element name="{'_' || local-name(.)}">
      <xsl:attribute name="xpath" select="saxon:path()"/>
      <xsl:apply-templates select="@* | node()" mode="#current"/>
    </xsl:element>
  </xsl:template>
  
  
  <!--copy template-->
  <xsl:template match="node() | @*" mode="formEFL2formEL" priority="-3">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <!--==================================================================================================-->
  <!--MODE optimizeFormEL-->
  <!--==================================================================================================-->
  
  <xsl:template match="Date[count(*) = 1]/Al" mode="optimizeFormEL">
    <xsl:apply-templates mode="#current"/>
  </xsl:template>
  
  <xsl:template match="Blocajouts/Al | Blocsousajouts/Al" mode="optimizeFormEL">
    <Alajout>
      <xsl:apply-templates select="@* | node()" mode="#current"/>
    </Alajout>
  </xsl:template>
  
  <xsl:template match="Blocajouts/Altiret" mode="optimizeFormEL">
    <Altiretajout>
      <xsl:apply-templates select="@* | node()" mode="#current"/>
    </Altiretajout>
  </xsl:template>
  
  <xsl:template match="Blocsousajouts/Altiret" mode="optimizeFormEL">
    <Alsoustiretajout>
      <xsl:apply-templates select="@* | node()" mode="#current"/>
    </Alsoustiretajout>
  </xsl:template>
  
  <xsl:template match="Blocoptions/Al | Blocsousoptions/Al" mode="optimizeFormEL">
    <Aloption>
      <xsl:apply-templates select="@* | node()" mode="#current"/>
    </Aloption>
  </xsl:template>
  
  <xsl:template match="node() | @*" mode="optimizeFormEL">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <!--==================================================================================================-->
  <!--COMMON-->
  <!--==================================================================================================-->
  

  
  
  
  
</xsl:stylesheet>