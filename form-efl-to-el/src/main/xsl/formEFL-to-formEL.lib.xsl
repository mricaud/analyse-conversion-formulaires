<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
  xmlns:els="http://www.lefebvre-sarrut.eu/ns/els"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  exclude-result-prefixes="#all"
  version="3.0">
  
  
  <!--Matche les GRCHOIX1, GRCHOIX2, GRCHOIX3, ..., GRCHOIX5 sauf GRCHOIXT-->
  <xsl:function name="els:isGRCHOIX" as="xs:boolean">
    <xsl:param name="e" as="element()"/>
    <xsl:sequence select="starts-with(name($e), 'GRCHOIX') and not(name($e) = 'GRCHOIXT')"/>
  </xsl:function>
  
  <!--Matche les CHOIX1, CHOIX2, CHOIX3, ..., CHOIX5 sauf CHOIXT-->
  <xsl:function name="els:isCHOIX" as="xs:boolean">
    <xsl:param name="e" as="element()"/>
    <xsl:sequence select="starts-with(name($e), 'CHOIX') and not(name($e) = 'CHOIXT')"/>
  </xsl:function>
  
  <!--Indique s'il s'agit de GRCHOIX de type choix multiple (checkbox) ou unique (radio)-->
  <xsl:function name="els:eflGRCHOIXisMultiple" as="xs:boolean">
    <xsl:param name="e" as="element()"/><!--GRCHOIX1, GRCHOIX2,...-->
    <xsl:choose>
      <!--Type multiple avec plusieurs choix-->
      <xsl:when test="$e/@TYPE='M'
        and count($e/*[starts-with(name(), 'CHOIX')]) gt 1">
        <xsl:sequence select="true()"/>
      </xsl:when>
      <!--Type unique mais un seul choix 
        => ça devient un booléen oui/non, donc plutôt une case à cocher <=> type multiple-->
      <xsl:when test="$e/@TYPE='U'
        and count($e/*[starts-with(name(), 'CHOIX')]) = 1">
        <xsl:sequence select="true()"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:sequence select="false()"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>
  
  <!--
    12 types de documents EFL => types EL  
      - Commentaires => Type 1
      - Avertissements => Type 1
      - Assignations => Type 1
      - Clausiers => Type 1
      - Contrats => Type 1
      - Statuts => Type 1
      - Rapports – Avis de publicité => Type 1
      - Pouvoirs => Type 1
      - Textes des résolutions => Type 1
      - Procès-verbal => Type 1
      - Divers => Type 1
      - Lettres => Type 2
      - N'EXISTE PAS AUX EFL => Schéma, Cerfa (Type 5)
  -->
  <xsl:function name="els:getACTEtypeEFL" as="xs:string">
    <xsl:param name="ACTE" as="element(ACTE)"/>
    <xsl:choose>
      <xsl:when test="$ACTE/LETTRE">
        <xsl:sequence select="'lettre'"/>
      </xsl:when>
      <xsl:otherwise>
        <!--FIXME-->
        <xsl:sequence select="'autre'"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>
  
  <!--Mapping entre le VAR/@CODE EFL et Precision/@Carnet EL-->
  <xsl:function name="els:varCode-to-PrecisionCarnet" as="xs:string">
    <xsl:param name="code" as="xs:string"/>
    <xsl:choose>
      <xsl:when test="starts-with($code, 'denomination_sociale__')">
        <xsl:text>dénomination sociale</xsl:text>
      </xsl:when>
      <xsl:when test="starts-with($code, 'montant__capital_social_de_la_societe')">
        <xsl:text>capital</xsl:text>
      </xsl:when>
      
      <xsl:when test="starts-with($code, 'numero_d_immatriculation_au_rcs__')">
        <xsl:text>RCS</xsl:text>
      </xsl:when>
      
      
      <xsl:when test="starts-with($code, 'nom_de_chaque_associe_consulte_par_ecrit_')">
        <xsl:text>nom du détenteur de capitaux</xsl:text><!--FIXME-->
      </xsl:when>
      <!--<xsl:when test="starts-with($code, 'numero_et_rue__adresse_de_chaque_associe_consulte_par_ecrit__')">
        <xsl:text>adresse</xsl:text>
      </xsl:when>-->
      
      <!--Pas d'équivalent dans le carnet EL-->
      <xsl:when test="starts-with($code, 'numero_et_rue__')">
        <xsl:text>adresse</xsl:text>
      </xsl:when>
      <xsl:when test="starts-with($code, 'code_postal__')">
        <xsl:text>?</xsl:text>
      </xsl:when>
      <xsl:when test="starts-with($code, 'ville__')">
        <xsl:text>?</xsl:text>
      </xsl:when>
      <xsl:when test="starts-with($code, 'ville_d_immatriculation_au_rcs__')">
        <xsl:text>?</xsl:text>
      </xsl:when>
      
      <!--Equivalent vide (pas de @Carnet)-->
      <xsl:when test="starts-with($code, 'date_de_la_lettre__consultation_de_chaque_associe__')">
        <xsl:text/>
      </xsl:when>
      <xsl:when test="starts-with($code, 'numero_article_statuts_relatif_consultation_par_correspondance_associes_')">
        <xsl:text/>
      </xsl:when>
      
      <xsl:otherwise>
        <xsl:sequence select="'FIXME:'||$code"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>
  
</xsl:stylesheet>